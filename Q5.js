// không thể sử dụng await bên trong forEach
// Có thể break thoát khỏi nó bằng cách sử dụng từ khóa break
async function run() {
  const arr = ['a', 'b', 'c'];
  arr.forEach(el => {
    // SyntaxError
    await new Promise(resolve => setTimeout(resolve, 1000));
    console.log(el);
  });
}
// nhưng trong for thì ok 
// Không thể break thoát khỏi vòng lặp
async function asyncFn() {
  const arr = ['case1', 'case2', 'case3'];
  for (const el of arr) {
    await new Promise(resolve => setTimeout(resolve, 1000));
    console.log(el);
  }
}

const main = async ( ) => {
     const a = await asyncFn()
}

main()

