


const main = async () => {
    try {
        const promises = []

        for (let i = 0; i < 20; i++) {
            let promiseResolve = new Promise((resolve, reject) => {
                resolve(`resolve Promise ${i}`);
            });

            promises.push(promiseResolve)
        }

        const run: any = []

        for (let i = 0; i < promises.length; i++) {
            if (!promises[i]) continue

            if (i === 0 && i % 5 !== 0 || i === promises.length - 1) {
                run.push(promises[i].catch(e => { if (e) { console.log(e) } }))
                continue
            }

             await Promise.all(run)

            run.length = []
        }

    } catch (e) {
        throw e
    }
}
main()

