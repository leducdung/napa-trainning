Khi nào stack trống, tức là hết tick, nó được thực hiện luôn

process.nextTick() thực ra là cách để thực hiện lời gọi hàm callback ngay lập tức. Hàm callback trong setImmediate() sẽ được thực thi trong vòng lặp tiếp theo

a được gọi trước b vì nextTick thực hiện lời gọi hàm callback ngay lập tức 


Lỗi Zalgo xảy ra khi lập trình viên trộn lẫn synchronous call back với asynchronous call back trong control flow : if then else

có thể Sửa lỗi Zalgo bằng process.nextTick

function getData(useCache, callback) {
    var fun = " Hello World";
    if (useCache) {
        process.nextTick(function(){
            callback('cached data' + fun);
        });
    } else {
        setTimeout(function(){
            callback('loaded data' + fun);
        }, 1000);

    }
}

console.log("Do task A");
getData(true, function(data){
    processData(data);
});
console.log("Do task C");

function processData(data) {
    console.log('processData', data);
}