let promise1 = new Promise ( (resolve, reject) => {
    resolve('Promise 1');
});

let promise2 = new Promise ( (resolve, reject) => {
    reject('reject Promise 2');
});

let promise3 = new Promise ( (resolve, reject) => {
    resolve('Promise 3');
});

 
const testFunc = async () => {
    const value = await Promise.all([promise1, promise2, promise3].map(p => {
        return p.then(result => {
            return { status: 'ok', value: result }
        }).catch(e => {
            return { status: "rejected", value: e }
        });

    }));
    console.log(value);
}

testFunc();