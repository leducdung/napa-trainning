const waitBlocking = ( milliseconds) => {
    const startTime = new Date().getTime();
    while (new Date().getTime() < startTime + milliseconds){}
}

const waitNonBlocking = ( milliseconds) => {
    return new Promise((resolve) => setTimeout(() => resolve()), milliseconds)
}

const controller1 = async (req, res) => {
    await waitNonBlocking(10000)
    res.status(200).end()
}

const controller2 = async (req, res) => {
    process.nextTick(function(){
        waitBlocking(10000)
    });
    res.status(200).end()
}