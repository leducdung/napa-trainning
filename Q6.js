Có 3 cách clone object

# Cách 1: Object.assign  
const obj = {a:1,b:2,c:3};

const clone = Object.assign({},obj);

//obj = {a:1,b:2,c:3};
# Cách 2: Spread Operator Es6 dấu ...  
const obj = {a:1,b:2,c:3};

const clone = {...obj};

// obj = {a:1,b:2,c:3};
# Cách 3: Sử dụng JSON.parse() và JSON.stringify()
const obj = {a:1,b:2,c:3};

const clone = JSON.parse(JSON.stringify(obj));

// clone = {a:1,b:2,c:3};   
deep : chùng giống shallow nhưng các giá trị reference trong object gốc không thay trong object clone.
shallow: khi chúng ta thay đổi giá trị từ 1 trong 2 biến, giá trị của biến còn lại sẽ thay đổi. các giá trị lồng nhau vẫn sử dụng reference đến một đối tượng ban đầu


sử dụng deep copy JSON.parse() và JSON.stringify() đó là đôi khi bị miss những tham số của bạn, nếu tham số đó bạn gán underfined hoặc NaN...
không nên sử dụng deep nếu có giá trị underfined hoặc NaN